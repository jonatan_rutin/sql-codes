truncate table DWH.marketing.COHORT_USER_RET_CG;
insert into DWH.marketing.COHORT_USER_RET_CG

with first_step as
  (select a.*,
         ses_date,
         lag(ses_date) over(partition by userid,engageindex order by ses_date) as previous_ses_date,
         lead(ses_date) over(partition by userid,engageindex order by ses_date) as next_ses_date,
         case when date(next_mts) = ses_date then datediff(day,previous_ses_date,ses_date) end as inactive_days_until_next_att,


         datediff(day,cohort_sdate,ses_date) as ret_day
  from 
      (select * 
      from "DWH"."MARKETING"."COHORT_USER") a
      left join
      (select user_id,
              app_id,
              date ses_date
      from "DWH"."EVENTS"."EVENTS_V_LOGIN"
      group by 1,2,3) b

  on a.userid = b.user_id
  and a.appid = b.app_id
  and b.ses_date >= date(a.mts)
  )
,second_step as
(select *,
       max(inactive_days_until_next_att) over(partition by userid,engageindex order by ses_date) as inactive_days_until_next_att_final
 from first_step)
,third_step as
(select *,
        lag(inactive_days_until_next_att_final) over (partition by userid order by engageindex,ses_date) inactive_days_until_next_att_final_leg
 from second_step)
 ,fourth_step as
 (select *,
         first_value(inactive_days_until_next_att_final_leg) over(partition by userid,engageindex order by ses_date) as inactive_days_until_attributed
  from third_step)
 
 select * from fourth_step
;