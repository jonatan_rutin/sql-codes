------Define incremental time-------
set v_from_date = current_date - 2 -- Incremental time
;
------Delete from base table-------
delete from marketing.V_EVENTS_INAPP_table
where date(created_at) >= $v_from_date
;
-------------------Last row for each adid in the base table----------------
create or replace temporary table marketing.V_EVENTS_INAPP_last_log_adid as
select *
from
(select *,
        row_number() over(partition by adid order by created_at desc) roww
from marketing.V_EVENTS_INAPP_table)
where roww = 1
;

-----Inc first step - Table as base, but only after the date which was deleted---------------------

create or replace temporary table marketing.V_EVENTS_INAPP_inc_first_step as
select *,
       MAX(LOG_SET_USERID) OVER (PARTITION BY adid,grouper) as userid,
       NULL as roww
from
    (select *,
           COUNT(LOG_SET_USERID) OVER (partition by adid ORDER BY created_at) as grouper
    from
        (select *,
               lead(set_userid) over(partition by adid order by created_at,event_name) as lead_userid, -- Passing the userid from event 'SET USER ID' to 'Login' in case that the login was few seconds beofre. 
               case when event_name in ('SET USER ID','Login') then nvl(set_userid,lead_userid) end as log_set_userid -- Define the userid which is exsit in one or both event 'SET USER ID' 'Login'. In case that the 'SET USER ID' came before, I cancelled the option of other prior event to get this user id.
        from
        
            (select app_id,
                   activity_kind,
                   TRACKER,
                   TRACKER_NAME,
                   NETWORK_NAME,
                   b.ad_adv,
                   case when lower(a.OS_NAME) like '%android%' then 1
                        when lower(a.OS_NAME) like 'ios' then 0
                        else -1
                        end platform,
                   IDFA_GPS_ADID,
                   IDFV,
                   adid,
                   CAMPAIGN_NAME,
                   CREATIVE_NAME,
                   adgroup_name as adset_id,
                   INSTALLED_AT,
                   CREATED_AT,
                   REATTRIBUTED_AT,
                   event_name,
                   case when event_name = 'SET USER ID' then substring(regexp_substr(Publisher_PARAMETERS, 'userid":"([0-9]+)'),10,20) end as set_userid
            from "DWH"."ADJUST"."EVENTS_INAPP" a
            left join mariadb.net2channel b
            on a.network_name = b.network
            where date(created_at) >= $v_from_date
            )
            
        )
   )
;
-----------------New Table Final - completing the null's userid with the last row of the base table. At the end, excluding this first row-------------------------------------------------------------
create or replace temporary table marketing.V_EVENTS_INAPP_inc_second_step as
select app_id,
       activity_kind,
       tracker,
       tracker_name,
       network_name,
       ad_adv,
       platform,
       idfa_gps_adid,
       idfv,
       adid,
       campaign_name,
       creative_name,
       adset_id,
       installed_at,
       created_at,
       reattributed_at,
       event_name,
       set_userid,
       lead_userid,
       log_set_userid,
       grouper,
       final_userid as userid
from
    (select *,
           case when userid is null then first_value(userid) over(partition by adid order by created_at)
           else userid end as final_userid,
           row_number() over(partition by adid order by created_at) roww_1
    from
      (
          (select * from marketing.V_EVENTS_INAPP_last_log_adid)
          union
          (select * from marketing.V_EVENTS_INAPP_inc_first_step)
          order by adid,created_at
      )
    )
where roww_1 > 1
;

----------Final Table - Union of the base with the new one--------------
create or replace temporary table marketing.V_EVENTS_INAPP as
(select * from V_EVENTS_INAPP_table)
union all
(select * from marketing.V_EVENTS_INAPP_inc_second_step)
;