create or replace temporary table marketing.V_EVENTS_INAPP_step_1 as
select app_id,
       activity_kind,
       TRACKER,
       TRACKER_NAME,
       NETWORK_NAME,
       b.ad_adv,
       case when lower(a.OS_NAME) like '%android%' then 1
            when lower(a.OS_NAME) like 'ios' then 0
            else -1
            end platform,
       IDFA_GPS_ADID,
       IDFV,
       adid,
       CAMPAIGN_NAME,
       CREATIVE_NAME,
       adgroup_name as adset_id,
       INSTALLED_AT,
       CREATED_AT,
       REATTRIBUTED_AT,
       event_name,
       case when event_name = 'SET USER ID' then substring(regexp_substr(Publisher_PARAMETERS, 'userid":"([0-9]+)'),10,20) end as set_userid
from "DWH"."ADJUST"."EVENTS_INAPP" a
left join mariadb.net2channel b
on a.network_name = b.network
;

------------------------------------------------------------------------------------------
create or replace temporary table marketing.V_EVENTS_INAPP_step_2 as
select *,
       lead(set_userid) over(partition by adid order by created_at,event_name) as lead_userid, -- Passing the userid from event 'SET USER ID' to 'Login' in case that the login was few seconds beofre. 
       case when event_name in ('SET USER ID','Login') then nvl(set_userid,lead_userid) end as log_set_userid -- Define the userid which is exsit in one or both event 'SET USER ID' 'Login'. In case that the 'SET USER ID' came before, I cancelled the option of other prior event to get this user id.  
from marketing.V_EVENTS_INAPP_step_1 
;

-------------------------------------------------------------------------------------------
truncate table marketing.V_EVENTS_INAPP_table;
insert into marketing.V_EVENTS_INAPP_table 
select *,
       MAX(LOG_SET_USERID) OVER (PARTITION BY adid,grouper) as userid
from
(select *,
       COUNT(LOG_SET_USERID) OVER (partition by adid ORDER BY created_at) as grouper
from marketing.V_EVENTS_INAPP_step_2
order by adid,created_at,event_name)
;
