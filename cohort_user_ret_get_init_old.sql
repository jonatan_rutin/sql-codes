truncate table DWH.marketing.COHORT_USER_RET_GE;
insert into DWH.marketing.COHORT_USER_RET_GE
select *,
       first_value(inactive_days_before_reatt) over(partition by userid,engageindex order by ses_date) as inactive_days_until_attributed
from
  (select a.*,
//         lag(engageindex) over(partition by userid order by ses_date) as previous_engageindex,
         lag(ses_date) over(partition by userid order by ses_date) as previous_ses_date,
         lead(ses_date) over(partition by userid order by ses_date) as next_ses_date,
         case when engageindex != previous_engageindex and previous_engageindex is not null then datediff(day,previous_ses_date,ses_date) end as inactive_days_before_reatt,
         b.ses_date,
         nvl(datediff(day,date(a.mts),b.ses_date),0) as ret_day
       
from
    (select *
    from "DWH"."MARKETING"."COHORT_USER") a
    left join
    (select user_id,
            app_id,
            date ses_date
    from "DWH"."EVENTS"."EVENTS_V_LOGIN"
    group by 1,2,3) b

    on a.userid = b.user_id
    and a.appid = b.app_id
    and ((b.ses_date >= date(a.mts) and b.ses_date < date(a.next_mts)) 
    or (b.ses_date >= date(a.mts) and a.next_mts is null)))

    order by 1,9
;